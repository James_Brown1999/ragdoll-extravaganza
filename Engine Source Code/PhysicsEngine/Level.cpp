#include "Level.h"



Level::Level()
{
}


Level::~Level()
{
}

void Level::CreateLevel1(PhysicsScene* scene)
{
	Rigidbody* levelObjects[11];
	levelObjects[0] = new BoxOBB(vec2(30, -34.5f), vec2(0, 0), 0, 400, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2.5f, 15), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[0]->setAngularDrag(15.f);
	levelObjects[0]->setLinearDrag(0.3f);
	levelObjects[0]->setMaxForce(100);
	levelObjects[1] = new BoxOBB(vec2(50, -16.4f), vec2(0, 0), 0, 400, vec4(160.0f / 255.0f, 82.0f / 255.0f, 45.0f / 255.0f, 1), vec2(30, 2.5f), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[1]->setAngularDrag(15.f);
	levelObjects[1]->setLinearDrag(0.3f);
	levelObjects[1]->setMaxForce(100);
	levelObjects[2] = new BoxOBB(vec2(70, -34.5f), vec2(0, 0), 0, 400, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2.5f, 15), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[2]->setAngularDrag(15.f);
	levelObjects[2]->setLinearDrag(0.3f);
	levelObjects[2]->setMaxForce(100);
	levelObjects[3] = new BoxOBB(vec2(30, 0), vec2(0, 5), 0, 100, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2.5f, 15), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[3]->setAngularDrag(15.f);
	levelObjects[3]->setLinearDrag(0.3f);
	levelObjects[3]->setMaxForce(100);
	levelObjects[4] = new BoxOBB(vec2(70, 5), vec2(0, 0), 0, 100, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2.5f, 15), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[4]->setAngularDrag(15.f);
	levelObjects[4]->setLinearDrag(0.3f);
	levelObjects[4]->setMaxForce(100);
	levelObjects[5] = new BoxOBB(vec2(50, 23), vec2(0, 0), 0, 100, vec4(160.0f / 255.0f, 82.0f / 255.0f, 45.0f / 255.0f, 1), vec2(30, 2.5f), vec2(0, 0), vec2(0, 0), false, false); //Need to go over contact Forces
	levelObjects[5]->setAngularDrag(15.f);
	levelObjects[5]->setLinearDrag(0.3f);
	levelObjects[5]->setMaxForce(100);
	levelObjects[6] = new BoxOBB(vec2(30, 45), vec2(0, 5), 0, 100, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2.5f, 15), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[6]->setAngularDrag(15.f);
	levelObjects[6]->setLinearDrag(0.3f);
	levelObjects[6]->setMaxForce(100);
	levelObjects[7] = new BoxOBB(vec2(70, 45), vec2(0, 0), 0, 100, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2.5f, 15), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[7]->setAngularDrag(15.f);
	levelObjects[7]->setLinearDrag(0.3f);
	levelObjects[7]->setMaxForce(100);
	levelObjects[8] = new BoxOBB(vec2(50, 65), vec2(0, 0), 0, 100, vec4(160.0f / 255.0f, 82.0f / 255.0f, 45.0f / 255.0f, 1), vec2(30, 2.5f), vec2(0, 0), vec2(0, 0), false, false); //Need to go over contact Forces
	levelObjects[8]->setAngularDrag(15.f);
	levelObjects[8]->setLinearDrag(0.3f);
	levelObjects[8]->setMaxForce(100);
	levelObjects[9] = new BoxOBB(vec2(-20, -40), vec2(0, 0), 0, 100, vec4(235.0f / 255.0f, 233.0f / 255.0f, 233.0f / 255.0f, 1), vec2(20, 10), vec2(0, 0), vec2(0, 0), true, false); //Need to go over contact Forces
	levelObjects[9]->setAngularDrag(15.f);
	levelObjects[9]->setLinearDrag(0.3f);
	levelObjects[9]->setMaxForce(100);
	levelObjects[10] = new BoxOBB(vec2(-20, 50), vec2(0, 0), 0, 100, vec4(1, 1, 1, 1), vec2(5, 20), vec2(0, 0), vec2(0, 0), true, false); //Need to go over contact Forces
	levelObjects[10]->setAngularDrag(15.f);
	levelObjects[10]->setLinearDrag(0.3f);
	levelObjects[10]->setMaxForce(100);

	for (int i = 0; i < 11; i++)
	{
		scene->addActor(levelObjects[i]);
	}
	beatScore = 300;
}

void Level::CreateLevel2(PhysicsScene * scene)
{
	Rigidbody* levelObjects[17];
	levelObjects[0] = new BoxOBB(vec2(10, -47), vec2(0, 0), 0, 50000, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(15, 3), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[0]->setAngularDrag(15.f);
	levelObjects[0]->setLinearDrag(0.3f);
	levelObjects[1] = new BoxOBB(vec2(40.1f, -33), vec2(0, 0), 0, 50000, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(15, 17), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[1]->setAngularDrag(15.f);
	levelObjects[1]->setLinearDrag(0.3f);
	levelObjects[2] = new BoxOBB(vec2(70.2f, - 47), vec2(0, 0), 0, 50000, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(15, 3), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[2]->setAngularDrag(15.f);
	levelObjects[2]->setLinearDrag(0.3f);
	levelObjects[3] = new BoxOBB(vec2(-3, -25), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 14), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[3]->setAngularDrag(15.f);
	levelObjects[3]->setLinearDrag(0.3f);
	levelObjects[4] = new BoxOBB(vec2(83.2, -25), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 14), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[4]->setAngularDrag(15.f);
	levelObjects[4]->setLinearDrag(0.3f);
	levelObjects[5] = new BoxOBB(vec2(10 , -8), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(15.1f, 2), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[5]->setAngularDrag(15.f);
	levelObjects[5]->setLinearDrag(0.3f);
	levelObjects[6] = new BoxOBB(vec2(70.2f, -8), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(15.1f, 2), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[6]->setAngularDrag(15.f);
	levelObjects[6]->setLinearDrag(0.3f);
	levelObjects[7] = new BoxOBB(vec2(-3, 15), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 14), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[7]->setAngularDrag(15.f);
	levelObjects[7]->setLinearDrag(0.3f);
	levelObjects[8] = new BoxOBB(vec2(83.2, 15), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 14), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[8]->setAngularDrag(15.f);
	levelObjects[8]->setLinearDrag(0.3f);
	levelObjects[9] = new BoxOBB(vec2(27.1f, 11), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 16), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[9]->setAngularDrag(15.f);
	levelObjects[9]->setLinearDrag(0.3f);
	levelObjects[10] = new BoxOBB(vec2(53.1f, 11), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 16), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[10]->setAngularDrag(15.f);
	levelObjects[10]->setLinearDrag(0.3f);
	levelObjects[11] = new BoxOBB(vec2(11, 35), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(16.1f, 2), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[11]->setAngularDrag(15.f);
	levelObjects[11]->setLinearDrag(0.3f);
	levelObjects[12] = new BoxOBB(vec2(69, 35), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(16.1f, 2), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[12]->setAngularDrag(15.f);
	levelObjects[12]->setLinearDrag(0.3f);
	levelObjects[13] = new BoxOBB(vec2(40, 37), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(12.8f, 2), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[13]->setAngularDrag(15.f);
	levelObjects[13]->setLinearDrag(0.3f);
	levelObjects[14] = new BoxOBB(vec2(27.1f, 57), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 12), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[14]->setAngularDrag(15.f);
	levelObjects[14]->setLinearDrag(0.3f);
	levelObjects[15] = new BoxOBB(vec2(53.1f, 57), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(2, 12), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[15]->setAngularDrag(15.f);
	levelObjects[15]->setLinearDrag(0.3f);
	levelObjects[16] = new BoxOBB(vec2(40, 77), vec2(0, 0), 0, 450, vec4(119.0f / 255.0f, 136.0f / 255.0f, 153.0f / 255.0f, 1), vec2(15, 2), vec2(0, 0), vec2(0, 0), false, false);
	levelObjects[16]->setAngularDrag(15.f);
	levelObjects[16]->setLinearDrag(0.3f);

	for (int i = 0; i < 17; i++)
	{
		scene->addActor(levelObjects[i]);
	}
	beatScore = 600;
}


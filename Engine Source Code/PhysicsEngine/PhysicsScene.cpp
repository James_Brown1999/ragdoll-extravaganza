#include "PhysicsScene.h"

PhysicsScene::~PhysicsScene()
{
	//Clean up and remove any actors in the scene being deleted
	for (auto pActor : m_actors)
	{
		delete pActor;
	}
}

void PhysicsScene::debugScene()
{
	//return the actor count.
	int count = 0;
	for (auto pActor : m_actors)
	{
		cout << count << ":";
		pActor->debug();
		count++;
	}
}

void PhysicsScene::addActor(PhysicsObject * actor)
{
	m_actors.push_back(actor);
}

void PhysicsScene::removeActor(PhysicsObject * actor)
{
	m_actors.erase(remove(m_actors.begin(), m_actors.end(), actor), m_actors.end()); //find the desired actor in scene and remove it.
	actorCount = m_actors.size(); //update actorCount
	resize = true;
}

void PhysicsScene::update(float deltaTime)
{
	if (deltaTime > 0.1) //Cap the update time
	{
		deltaTime = 0.1f;
	}
	static float accumulatedTime = 0.0f;
	accumulatedTime += deltaTime; //Add deltaTime to the current accumalted time.

	while (accumulatedTime >= m_timeStep) //once the accumalated time passed the fixed timestep, update its physics
	{
		for (auto pActor : m_actors)
		{
			pActor->fixedUpdate(m_gravity, m_timeStep);
		}		
		accumulatedTime -= m_timeStep; //remove accumalated time.
		checkCollision(); //check for any collisions.	
	}
}

void PhysicsScene::updateGizmos()
{
	for (auto pActor : m_actors) //Draw objects as genric shape.
	{
		pActor->makeGizmo();
	}
}

PhysicsObject * PhysicsScene::getActor(PhysicsObject * actor)
{
	auto desiredActor = find(m_actors.begin(), m_actors.end(), actor); //find the desited actor

	if (desiredActor != m_actors.end()) //if the desired actor isnt the last in the vector
	{
		int i = std::distance(m_actors.begin(), desiredActor); //get its position in the vector
		actor = m_actors[i]; //set parameter reference
		return actor; //return found actor
	}


	return nullptr; //no actor was found.
}



void PhysicsScene::clearScene()
{
	aie::Gizmos::clear();
	m_actors.clear();
}

typedef bool(*fn)(PhysicsObject*, PhysicsObject*); //Function pointer for collisions function array that takes two physics objects

static fn collisionFunctionArray[] =
{
	PhysicsScene::planeVsPlane,	PhysicsScene::planeVsCircle, PhysicsScene::planevsBoxAABB, PhysicsScene::planevsBoxOBB,
	PhysicsScene::circleVsPlane, PhysicsScene::circleVsCircle, PhysicsScene::circlevsBoxAABB, PhysicsScene::circlevsBoxOBB,
	PhysicsScene::boxAABBvsPlane, PhysicsScene::boxAABBvsCircle, PhysicsScene::boxAABBvsboxAABB, PhysicsScene::boxABBvsboxOBB,
	PhysicsScene::boxOBBvsPlane, PhysicsScene::boxOBBvsCircle, PhysicsScene::boxOBBvsboxAABB, PhysicsScene::boxOBBvsboxOBB
};




void PhysicsScene::checkCollision()
{
	actorCount = m_actors.size();

	for (int outer = 0; outer < actorCount - 1; outer++)
	{
		for (int inner = outer + 1; inner < actorCount; inner++) //comparing
		{
			if (resize) //when something is removed from the scene, then break out of the loop as the old actor count is still being passed in, despite changing it when the actors are removed.
			{
				break;
			}
			PhysicsObject* object1 = m_actors[outer];
			PhysicsObject* object2 = m_actors[inner];
			if (object1 != nullptr && object2 != nullptr)
			{
				int shapeId1 = object1->getShapeID(); //setting the current shape (i.e. circle)
				int shapeId2 = object2->getShapeID();

				if (shapeId1 < 0 || shapeId2 < 0)
				{
					continue;
				}

				int functionIdx = (shapeId1 * SHAPE_COUNT) + shapeId2; //Calculating the Index required.
				fn collisionFunctionPtr = collisionFunctionArray[functionIdx]; //set the current function
				if (collisionFunctionPtr != nullptr)
				{
					collisionFunctionPtr(object1, object2); //call it.
				}
			}
			
		}
	}
	if (resize) //after the loop has been broken after resizeing, set the need to be resized to false.
	{
		resize = false;
	}
}

bool PhysicsScene::planeVsPlane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return false;
}

bool PhysicsScene::planeVsCircle(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return circleVsPlane(obj1, obj2);
}

bool PhysicsScene::circleVsPlane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Circle* circle = dynamic_cast<Circle*>(obj1); //cast obj1 to circle
	Plane* plane = dynamic_cast<Plane*>(obj2); //cast obj2 to plane

	if (circle != nullptr && plane != nullptr)
	{
		vec2 collisionNormal = plane->getNormal(); //get the planes normal
		float circleToPlane = dot(circle->getPosition(), plane->getNormal()) - plane->getDistance(); //get the dot product between the plane and circle to check which side the circle is intersecting

		if (circleToPlane < 0) // if the  and plane point in opposite directions, inverse the normal and dot product.
		{
			collisionNormal *= -1;
			circleToPlane *= -1;
		}

		float intersection = circle->getRadius() - circleToPlane; //calculate the intersection point

		if (intersection > 0) //if the circle as intersected the plane
		{
			circle->setPosition(circle->getPosition() + plane->getNormal() * (circle->getRadius() - circleToPlane)); //Apply offset position

			vec2 contact = circle->getPosition() + (collisionNormal * -circle->getRadius()); //calulate contact point to pass to collision resolution
			plane->resolveCollision(circle, contact); //resolve collision.
			return true;
		}
	}

	return false;
}

bool PhysicsScene::circleVsCircle(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Circle* cir1 = dynamic_cast<Circle*>(obj1); //Cast obj1 to circle
	Circle* cir2 = dynamic_cast<Circle*>(obj2); //Cast obj2 to circle
	if (cir1 != nullptr && cir2 != nullptr)
	{
		float diffX = cir1->getPosition().x - cir2->getPosition().x; //Calculate distances
		float diffY = cir1->getPosition().y - cir2->getPosition().y;
		float distance = sqrt(diffX * diffX + diffY * diffY);
		float combinedRadius = cir1->getRadius() + cir2->getRadius(); //calculate the combined radius
		if (distance < combinedRadius) //if both circles are clipping each other
		{ 
			vec2 contactForce = 0.5f * (distance -(cir1->getRadius() + cir2->getRadius())) * (cir2->getPosition() - cir1->getPosition()) / distance; //calculate the contact force

			cir1->setPosition(cir1->getPosition() + contactForce); //Apply offset positions
			cir2->setPosition(cir2->getPosition() - contactForce);


			vec2 contact = 0.5f * (cir1->getPosition() + cir2->getPosition()); //calculate contact force.
			cir1->resolveCollision(cir2, contact); //resolve the collision.
			return true; //has collided
		}
	}
	return false;
}

bool PhysicsScene::boxAABBvsboxAABB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Box* aabb1 = dynamic_cast<Box*>(obj1); //Cast obj1 to AABB box
	Box* aabb2 = dynamic_cast<Box*>(obj2); //Cast obj2 to AABB box

	if (aabb1 != nullptr && aabb2 != nullptr) //if both objs were cast successfully.
	{
		//check if both boxes clip each other
		if (aabb1->getMin().x < aabb2->getMax().x &&
			aabb1->getMax().x > aabb2->getMin().x &&
			aabb1->getMin().y < aabb2->getMax().y &&
			aabb1->getMax().y > aabb2->getMin().y)
		{
			return true;
		}
	}
	return false;
}

bool PhysicsScene::planevsBoxAABB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	return boxAABBvsPlane(obj1,obj2);
}

bool PhysicsScene::boxAABBvsPlane(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Box* aabb = dynamic_cast<Box*>(obj1); //Cast obj1 to AABB box
	Plane* plane = dynamic_cast<Plane*>(obj2); //Cast obj2 to plane

	if (aabb != nullptr && plane != nullptr)
	{
		vec2 collisionNormal = plane->getNormal(); //get the planes normal
		float aabbToPlane = dot(aabb->getPosition(), plane->getNormal()) - plane->getDistance(); //get the dot product between the plane and box to check which side the box is intersecting the plane

		if (aabbToPlane < 0) //if the box is behind the plane, inverse the collision normal and dot product.
		{
			collisionNormal *= -1;
			aabbToPlane *= -1;
		}

		float intersectionSides = aabb->getWidth() - aabbToPlane; //get the intersecting horizontal sides (Left or Right)
		float intersectionTops = aabb->getHeight() - aabbToPlane; //get the intersecting vertical sides (Top or Bottom)
		if (intersectionSides > 0 || intersectionTops > 0)
		{		
			//plane->resolveCollision(aabb);
			return true;
		}
	}

	return false;
}


bool PhysicsScene::circlevsBoxAABB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Circle* cir = dynamic_cast<Circle*>(obj1); //Cast obj1 to a circle
	Box* aabb = dynamic_cast<Box*>(obj2); //Cast obj2 to a AABB box
	
	if (aabb != nullptr && cir != nullptr)
	{
		float diffX = aabb->getPosition().x - cir->getPosition().x + aabb->getWidth(); //Calculate distances
		float diffY = aabb->getPosition().y - cir->getPosition().y + aabb->getHeight();
		float distance = sqrt(diffX * diffX + diffY * diffY);
		if (distance < cir->getRadius()) //if the distance is less than the circles radius, objects are colliding.
		{
			return true;
		}
	}

	return false;
}

bool PhysicsScene::boxAABBvsCircle(PhysicsObject * obj1, PhysicsObject * obj2)
{
	return circlevsBoxAABB(obj1, obj2);
}

bool PhysicsScene::boxOBBvsboxOBB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	BoxOBB* box1 = dynamic_cast<BoxOBB*>(obj1); //Cast obj1 to OBB box
	BoxOBB* box2 = dynamic_cast<BoxOBB*>(obj2); //Cast obj2 to OBB box

	if (box1 != nullptr && box2 != nullptr)
	{
		vec2 norm(0, 0);
		vec2 contact(0, 0);
		float penetration = 0;
		int numContacts = 0;

		box1->checkBoxCorners(*box2, contact, numContacts, penetration, norm); //check box1 for collision
		if (box2->checkBoxCorners(*box1, contact, numContacts, penetration, norm)) //if resolution occurs with box2
		{
			norm = -norm; //inverse the norm
		}
		if (penetration > 0) //if penetration occurs resolve collision
		{
			box1->resolveCollision(box2, contact / float(numContacts), &norm); //resolve box1 collision.
			ApplyContactForces(box1, box2, norm, penetration); //apply contact forces between the boxes
			float forceMag = abs(contact.x * contact.y); //return the absolute value of the contact force vector.
			//DESTROYING THE RAGDOLL
			//if the limb (torso, arms or legs) is still attached to the body
			if (box2->getParent() != nullptr)
			{
				//if one piece is a ragdoll and the other isnt, collide and is killable and is crushed by enough force
				if (!box1->isRagDollPart() && box2->isRagDollPart() && box2->getParent()->isDestructible() && forceMag > 1500)
				{
					box2->getParent()->destroyJoints(box2->getParent()->getScene()); //destroy the joints (decided to go with this as its funnier than just deleting the whole body from the scene)
				}
			}		
		}
		return true;
	}
	return false;
}

bool PhysicsScene::boxOBBvsboxAABB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	return false;
}

bool PhysicsScene::boxABBvsboxOBB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	return false;
}

bool PhysicsScene::planevsBoxOBB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	return boxOBBvsPlane(obj1, obj2);
}

bool PhysicsScene::boxOBBvsPlane(PhysicsObject * obj1, PhysicsObject * obj2)
{
	BoxOBB* boxOBB = dynamic_cast<BoxOBB*>(obj1); //Cast obj1 to OBB box
	Plane* plane = dynamic_cast<Plane*>(obj2); //Cast obj2 to plane

	if (boxOBB != nullptr && plane != nullptr)
	{
		int numContacts = 0;
		vec2 contact(0, 0);
		float contactV = 0;
		float radius = 0.5f * fminf(boxOBB->getWidth(), boxOBB->getHeight());
		float penetration = 0;

		//which side the boxs center of mass is from the plane on
		vec2 planeOrigin = plane->getNormal() * plane->getDistance();
		float comFromPlane = dot(boxOBB->getPosition() - planeOrigin,
			plane->getNormal());

		// check all four corners to see if we've hit the plane
		for (float x = -boxOBB->getExtents().x; x < boxOBB->getWidth(); x += boxOBB->getWidth())
		{
			for (float y = -boxOBB->getExtents().y; y < boxOBB->getHeight(); y += boxOBB->getHeight())
			{
				//get the position of the corner in world space
				vec2 p = boxOBB->getPosition() + x * boxOBB->getLocalX() + y * boxOBB->getLocalY();

				//calculate which side the the extent of the box is on and how far it is from the plane.
				float distFromPlane = dot(p - planeOrigin, plane->getNormal());

				//total velocity of this point
				float velocityIntoPlane = dot(boxOBB->getVelocity() +
					boxOBB->getAngularVelocity() * (-y * boxOBB->getLocalX() + x * boxOBB->getLocalY()), plane->getNormal());

				//if this corner is coming from the right or lect side of the plane,
				//resolve collision
				if ((distFromPlane > 0 && comFromPlane < 0 && velocityIntoPlane > 0) ||
					(distFromPlane < 0 && comFromPlane > 0 && velocityIntoPlane < 0))
				{
					numContacts++;
					contact += p;
					contactV += velocityIntoPlane; //set contact velocity

					if (comFromPlane >= 0) //if coming from the right side of the plane
					{
						if (penetration > distFromPlane)
						{
							penetration = distFromPlane;
						}
						else 
						{
							if (penetration < distFromPlane)
							{
								penetration = distFromPlane;
							}
						}
					}
				}
			}
		}
		//we've had a hit - usually only two corners can contact.
		if (numContacts > 0)
		{
			//get the average collision velocity into the plane
			//(covers both linear and angular velocity of all corners)
			float collisionV = contactV / (float)numContacts;

			//get the acceleration needed to stop (restitution = 0, bodys wont bounce after collision) or reverse
			//(restitution = 1, bodys will bounce) the average velocity into the plane
			vec2 acceleration = -plane->getNormal() *
				((1.0f + boxOBB->getElasticity()) * collisionV);
			//the average position at which well apply the force (corner or the center of mass of the corner)
			vec2 localContact = (contact / (float)numContacts) - boxOBB->getPosition();
			//the perpendicular distance we apply the force at relative to the center of mass (Torque = F/r)
			float r = dot(localContact, vec2(plane->getNormal().y,
				-plane->getNormal().x));

			//working out the effective mass - this is a combination of moment of
			//inertia(resistance) and mass, and tells us how much the contact point velocity
			//will change with the force we're applying
			float mass0 = 1.0f / (1.0f / boxOBB->getMass() + (r * r) / boxOBB->getMoment()); // 1/1/Ma + r^2 / boxs moment

			boxOBB->applyForce(acceleration * mass0, localContact); //apply forces
			boxOBB->setPosition(boxOBB->getPosition() - plane->getNormal() * penetration); //set offset positions
		}
	}

	return false;
}

bool PhysicsScene::circlevsBoxOBB(PhysicsObject * obj1, PhysicsObject * obj2)
{
	return boxOBBvsCircle(obj1, obj2);
}

bool PhysicsScene::boxOBBvsCircle(PhysicsObject * obj1, PhysicsObject * obj2)
{
	BoxOBB* boxOBB = dynamic_cast<BoxOBB*>(obj2); //Cast obj1 to OBB box
	Circle* cir = dynamic_cast<Circle*>(obj1); //Cast obj2 to circle

	if (boxOBB != nullptr && cir != nullptr)
	{
		vec2 cirPos = cir->getPosition() - boxOBB->getPosition(); //circles position relative to the box
		float w2 = boxOBB->getWidth() / 2; //widths half extents
		float h2 = boxOBB->getHeight() / 2; //heights half extents

		int numContacts = 0;
		vec2 contact(0, 0);

		for (float x = -w2; x <= w2; x += boxOBB->getWidth())
		{
			for (float y = -h2; y <= h2; y += boxOBB->getHeight())
			{
				vec2 p = x * boxOBB->getLocalX() + y * boxOBB->getLocalY(); //point on the box
				vec2 dp = p - cirPos; //distance of the point from the circle
				if (dp.x*dp.x + dp.y*dp.y < cir->getRadius() * cir->getRadius()) //if distance point is less than circle radius
				{
					numContacts++;
					contact += vec2(x, y); //set the contact point
				}
			}
		}
		vec2* direction = nullptr;
		vec2 localPos(dot(boxOBB->getLocalX(), cirPos),
			dot(boxOBB->getLocalY(), cirPos)); //localposition of the circle pos relative to the box
		if (localPos.y < h2 && localPos.y > -h2) //if the local position is less than the half extents + radius (so inside the box)
		{
			if (localPos.x > 0 && localPos.x < w2 + cir->getRadius()) //if its within the right side of the box
			{
				numContacts++;
				contact += vec2(w2, localPos.y);
				direction = new vec2(boxOBB->getLocalX());
			}
			if (localPos.x < 0 && localPos.x > -(w2 + cir->getRadius())) //if its within the left side of the box
			{
				numContacts++;
				contact += vec2(-w2, localPos.y);
				direction = new vec2(-boxOBB->getLocalX());
			}
		}
		if (localPos.x < w2 && localPos.x > -w2)//if the local position is less than the half extents + radius (so inside the box)
		{
			if (localPos.y > 0 && localPos.y < h2 + cir->getRadius()) //if its within the top side of the box
			{
				numContacts++;
				contact += vec2(localPos.x, h2);
				direction = new vec2(boxOBB->getLocalY());
			}
			if (localPos.y < 0 && localPos.y > -(h2 + cir->getRadius())) //if its within the bottom side of the box
			{
				numContacts++;
				contact += vec2(localPos.x, -h2);
				direction = new vec2(-boxOBB->getLocalY());
			}
		}
		if (numContacts > 0)
		{
			//calculate contact point
			contact = boxOBB->getPosition() + (1.0f / numContacts) *
				(boxOBB->getLocalX() * contact.x + boxOBB->getLocalY()*contact.y); //Pa + (1/numOfContacts) / (PLa.x * contactpointX + PLa.y *contactPointY). (PLa = local point for object a) 


			float pen = cir->getRadius() - length(contact - cir->getPosition()); //calculate the penetration with the radius - the distance between point of contact and circles center
			vec2 norm = normalize(cir->getPosition() - contact); // normalize
			boxOBB->resolveCollision(cir, contact, direction); //resolve collision between the circle and the box.

			ApplyContactForces(boxOBB, cir, norm, pen); //apply contact forces
			float forceMag = abs(contact.x * contact.y); //The absolute value of the contact forces vector

			//Destroy Ragdoll
			//if the limb (in this case the head) is still attached to the body
			if (cir->getParent() != nullptr)
			{
				//if one piece is a ragdoll and the other isnt, collide and is killable and is crushed by enough force
				if (cir->isRagDollPart() && !boxOBB->isRagDollPart() && cir->getParent()->isDestructible() && forceMag > 1500)
				{
					cir->getParent()->destroyJoints(cir->getParent()->getScene());  //destroy the joints (decided to go with this as its funnier than just deleting the whole body from the scene)
				}
			}
			
		}
		delete direction; //remove the direction when done
	}

	return false;
}

void PhysicsScene::ApplyContactForces(Rigidbody* body1, Rigidbody* body2, vec2 normal, float penetration)
{
	//is body1 or body2 a kinematic object
	float body1Factor = body1->isKinematic() ? 0 : (body2->isKinematic() ? 1.0f : 0.5f);
	body1->setPosition(body1->getPosition() - body1Factor * normal * penetration);
	body2->setPosition(body2->getPosition() + (1 - body1Factor) * normal * penetration);
}

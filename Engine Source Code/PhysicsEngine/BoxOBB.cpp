#include "BoxOBB.h"





void BoxOBB::fixedUpdate(vec2 gravity, float timeStep)
{
	Rigidbody::fixedUpdate(gravity, timeStep);

	float cs = cosf(m_angle);
	float sn = sinf(m_angle);
	m_localX = normalize(vec2(cs, sn));
	m_localY = normalize(vec2(-sn, cs));
}

void BoxOBB::makeGizmo()
{
	vec2 p1 = m_position - m_localX * m_extents.x - m_localY * m_extents.y;
	vec2 p2 = m_position + m_localX * m_extents.x - m_localY * m_extents.y;
	vec2 p3 = m_position - m_localX * m_extents.x + m_localY * m_extents.y;
	vec2 p4 = m_position + m_localX * m_extents.x + m_localY * m_extents.y;
	aie::Gizmos::add2DTri(p1, p2, p4, m_colour);
	aie::Gizmos::add2DTri(p1, p4, p3, m_colour);
}

bool BoxOBB::checkCollision(PhysicsObject * pOther)
{
	return false;
}

bool BoxOBB::checkBoxCorners(const BoxOBB& box, vec2 & contact, int & numContacts, float & penetration, vec2 & edgeNormal)
{
	float minX; float maxX; float minY; float maxY;
	float boxW = box.getWidth();
	float boxH = box.getHeight();
	int numOfLocalContacts = 0;
	vec2 localContact(0, 0);

	bool first = true;
	for (float x = -box.getExtents().x; x < boxW; x += boxW)
	{
		for (float y = -box.getExtents().y; y < boxH; y += boxH)
		{
			//position in worldSpace
			vec2 pos = box.getPosition() + x * box.m_localX + y * box.m_localY;
			
			//position in boxSpace
			vec2 pos0(dot(pos - m_position, m_localX),
				dot(pos - m_position, m_localY));

			if (first || pos0.x < minX)
			{
				minX = pos0.x;
			}
			if (first || pos0.x > maxX)
			{
				maxX = pos0.x;
			}
			if (first || pos0.y < minY)
			{
				minY = pos0.y;
			}
			if (first || pos0.y > maxY)
			{
				maxY = pos0.y;
			}

			if (pos0.x >= -m_extents.x && pos0.x <= m_extents.x &&
				pos0.y >= -m_extents.y && pos0.y <= m_extents.y)
			{
				numOfLocalContacts++;
				localContact += pos0;
			}
			first = false;
		}
	}

	if (maxX < -m_extents.x || minX > m_extents.x ||
		maxY < -m_extents.y || minY > m_extents.y) //if the mins and maxs are not within the extents
	{
		return false;
	}
	if (numOfLocalContacts == 0) //or not contacts
	{
		return false;
	}

	bool resolution = false;

	contact += m_position + (localContact.x * m_localX + localContact.y * m_localY) / (float)numOfLocalContacts; //setting the contact position
	numContacts++;

	float pen0 = m_extents.x - minX; //penetration in box space. This to the left side of the box
	if (pen0 > 0 && (pen0 < penetration || penetration == 0)) //if local pen is greator than 0 and local pen is less than current penetration or hasnt got penetration
	{
		edgeNormal = m_localX;
		penetration = pen0;
		resolution = true;
	}
	pen0 = maxX + m_extents.x; //penetration to the right side of the box
	if (pen0 > 0 && (pen0 < penetration || penetration == 0))
	{
		edgeNormal = -m_localX;
		penetration = pen0;
		resolution = true;
	}
	pen0 = m_extents.y - minY; //penetration at the bottom
	if (pen0 > 0 && (pen0 < penetration || penetration == 0))
	{
		edgeNormal = m_localY;
		penetration = pen0;
		resolution = true;
	}
	pen0 = maxY + m_extents.y; //penetration at the top
	if (pen0 > 0 && (pen0 < penetration || penetration == 0))
	{
		edgeNormal = -m_localY;
		penetration = pen0;
		resolution = true;
	}
	return resolution; //return if a resolution needs to occur
}

bool BoxOBB::isInside(vec2 point)
{
	point -= m_position;
	vec2 boxPt(dot(point, m_localX), dot(point, m_localY));
	if (fabs(boxPt.x) < getWidth() && fabs(boxPt.y) < getHeight())
	{
		return true;
	}

	return false;
}

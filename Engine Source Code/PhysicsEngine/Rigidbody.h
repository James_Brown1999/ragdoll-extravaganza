#pragma once
#include "PhysicsObject.h"
#include <vector>
#include <glm/vec2.hpp>
#include <glm/ext.hpp>
#include <list>

//Minimum linear velocity
#define MIN_LINEAR_THRESHOLD 0.1f
//Minimum angular
#define MIN_ROTATION_THRESHOLD 0.01f

class RagDoll;

using namespace std;
using namespace glm;


class Rigidbody : public PhysicsObject
{
public:

	Rigidbody(ShapeType shapeId, vec2 position,
		vec2 velocity, float angle, float mass, float angularVelocity, bool kinematic, bool ragdoll) : PhysicsObject(shapeId), m_position(position), m_velocity(velocity),
		m_angle(angle), m_mass(mass), m_angularVelocity(angularVelocity), m_isKinematic(kinematic), m_ragDollPart(ragdoll) {
		m_startPosition = m_position;
	}
	~Rigidbody() {}

	virtual void fixedUpdate(vec2 gravity, float timeStep);
	virtual void debug();
	void applyForce(vec2 force, vec2 pos); //apply force
	virtual bool checkCollision(PhysicsObject* pOther) = 0; //defined and overwritten in the collider classes that derive from rigidbody (i.e. boxOBB, Circle)

	void resolveCollision(Rigidbody* actor2, vec2 contact, vec2* collisionNormal = nullptr); //resolves collisions
	//////////GETS AND SETS//////////
	vec2 getPosition() const { return m_position; }
	vec2 setPosition(vec2 position) { return m_position = position; }
	float getAngle() const { return m_angle; }
	vec2 setVelocity(vec2 newVelocity) { return m_velocity = newVelocity; }
	vec2 getVelocity() const { return m_velocity; }
	float setAngle(float angle) {return m_angle = angle; }
	float setAngularVelocity(float angleVelocity) { return m_angularVelocity = angleVelocity; }
	float getAngularVelocity() const { return m_angularVelocity; }
	float getMass() const { return m_mass; }//if kinematic have an infinite mass. (or the highest int number)
	float getLinearDrag() const { return m_linearDrag; }
	float setLinearDrag(float linearDrag) { return m_linearDrag = linearDrag; }
	float getAngularDrag() const { return m_angularDrag; }
	float setAngularDrag(float angularDrag) { return m_angularDrag = angularDrag; }
	float getElasticity() const { return m_elasticity; }
	float setMoment(float moment) { return m_moment = moment; }
	float getMoment() const { return m_moment; }
	bool isKinematic() const { return m_isKinematic; }
	bool setKinematic(bool state) { return m_isKinematic = state; }
	bool isRagDollPart() const { return m_ragDollPart; } //checks if the rigidbody is a part of a ragdoll body.
	vec2 getLocalX() const { return m_localX; }
	vec2 getLocalY() const { return m_localY; }
	vec2 setLocalX(vec2 localX) { return m_localX = localX; }
	vec2 setLocalY(vec2 localY) { return m_localY = localY; }
	vec2 toWorld(vec2 coor) { return m_position + (m_localX * coor.x + m_localY * coor.y); } //Convert local coords to global coords
	float setMaxForce(float maxForce) { return m_maxForce = maxForce; }
	void resetPosition(); //reset rigidbodys position to its original spawn position
	float getKineticEnergy() { return 0.5f * (m_mass * dot(m_velocity, m_velocity) + m_moment * m_angularVelocity * m_angularVelocity); } //get the kinetic energy the rigidbody currently has built
	RagDoll* setParent(RagDoll* parent) { return m_parent = parent; }
	RagDoll* getParent() { return m_parent; }

protected:
	vec2 m_position;
	vec2 m_velocity;
	float m_elasticity = 0.01f; //1 = Bouncy Boi
	float m_mass;
	float m_maxForce;
	float m_angle; //2D only requires single float.
	float m_linearDrag;
	float m_angularDrag;
	float m_angularVelocity;
	bool m_isKinematic = false;
	bool m_ragDollPart = false;
	float m_moment;
	bool m_textureSet = false;
	vec2 m_localX;
	vec2 m_localY;
	vec2 m_startPosition; //position of the object when the application opens up.
	RagDoll* m_parent = nullptr;
};


#pragma once
#include "PhysicsObject.h"
#include <glm/vec4.hpp>
#include <glm/ext.hpp>
#include <Gizmos.h>
#include "Rigidbody.h"

class Plane : public PhysicsObject
{
public:
	Plane(vec2 normal, float distance) : PhysicsObject(PLANE), m_normal(normalize(normal)), m_distanceToOrigin(distance) { 
	}
	~Plane() {}

	virtual void fixedUpdate(vec2 gravity, float timeStep);
	virtual void debug() {}
	virtual void makeGizmo();
	virtual void resetPosition();

	vec2 getNormal() { return m_normal; }
	float getDistance() { return m_distanceToOrigin; }

	void resolveCollision(Rigidbody* actor2, vec2 contact);

protected:
	vec2 m_normal;
	float m_distanceToOrigin;

};


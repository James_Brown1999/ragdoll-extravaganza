#pragma once
#include "Rigidbody.h"
#include <glm/vec4.hpp>
#include <Gizmos.h>
class Circle : public Rigidbody
{
public:
	Circle(vec2 position, vec2 velocity,float angle,
		float mass, float radius, vec4 colour, vec2 localX, vec2 localY,bool kinematic, bool ragDoll) : Rigidbody(CIRCLE, position, velocity, angle, mass, 0, kinematic, ragDoll), m_radius(radius),
		m_colour(colour) {
		setMoment(0.5f * mass * radius * radius);
		setLocalX(vec2(localX));
		setLocalY(vec2(localY));
	}
	~Circle() {}

	virtual void makeGizmo();
	virtual bool checkCollision(PhysicsObject* pOther);
	float getRadius() { return m_radius; }
	vec4 getColour() { return m_colour; }


protected:
	float m_radius;
	vec4 m_colour;
};


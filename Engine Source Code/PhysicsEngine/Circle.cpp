#include "Circle.h"


void Circle::makeGizmo()
{
	vec2 end = vec2(cos(m_angle), sin(m_angle) * m_radius);

	aie::Gizmos::add2DCircle(m_position, m_radius, 12, m_colour);
	aie::Gizmos::add2DLine(m_position, m_position + end, vec4(1, 1, 1, 1));
}

bool Circle::checkCollision(PhysicsObject * pOther)
{
	/*Circle* cir = dynamic_cast<Circle*>(pOther);
	if (cir)
	{
	
		float diffX = this->m_position.x - cir->m_position.x;
		float diffY = this->m_position.y - cir->m_position.y;
		float distance = sqrt(diffX * diffX + diffY * diffY);
		float combinedRadius = this->m_radius + cir->m_radius;
		if (distance < combinedRadius)
		{
			
			return true;
		}
	}*/
	return false;
}



#include "Rigidbody.h"

void Rigidbody::fixedUpdate(vec2 gravity, float timeStep)
{
	//if the body is kinematic, set linear and angular velocity to zero and exit update.
	if (m_isKinematic)
	{
		m_velocity = vec2(0, 0);
		m_angularVelocity = 0;
		return;
	}

	
	m_position += m_velocity * timeStep; //update position
	m_velocity += gravity * timeStep; //update velocity
	m_velocity -= m_velocity * m_linearDrag * timeStep; //Add drag
	m_angle += m_angularVelocity * timeStep; //update rotation velocity
	m_angularVelocity -= m_angularVelocity * m_angularDrag * timeStep; //update angular velocity

	if (length(m_velocity) < MIN_LINEAR_THRESHOLD) //if velocity is below threshold, set to zero
	{
		m_velocity = vec2(0, 0);
	}
	if (abs(m_angularVelocity) < MIN_ROTATION_THRESHOLD) //if angular velocity is below threshold, set to zero
	{
		m_angularVelocity = 0;
	}

}

void Rigidbody::debug()
{
}

void Rigidbody::applyForce(vec2 force, vec2 pos)
{
	m_velocity += force / m_mass; //F=ma reorganised to a=F/m
	if (m_shapeID != CIRCLE) //since there is no friction, should be no reason for the circle to spin
	{
		//Preventing the boxes from getting angular velocity from nothing
		if (force.y < 6 && m_angle == 45 || m_angle == 90 || m_angle == 0)
		{
			force.y = 0;
		}
		m_angularVelocity += (force.y * pos.x - force.x * pos.y) / (m_moment);
	}	
}

void Rigidbody::resolveCollision(Rigidbody * actor2, vec2 contact, vec2* collisionNormal)
{
	//find the vector between their centres, or use the provided direction
	//of force, and make sure its normalized.
	vec2 normal = normalize(collisionNormal ? *collisionNormal :
		actor2->m_position - m_position);
	//get the vector perpendicular to the collision normal
	vec2 perp(normal.y, -normal.x);

	//determine the total velocity of the contact points for the two objects,
	//for both linear and rotational.

	// 'r' is the radius from axis to the point of force
	float r1 = dot(contact - m_position, -perp);
	float r2 = dot(contact - actor2->m_position, perp);
	//velocity of the contact point on this object
	float v1 = dot(m_velocity, normal) - r1 * m_angularVelocity;
	//velocity of contact point on actor2
	float v2 = dot(actor2->m_velocity, normal) + r2 * actor2->m_angularVelocity;

	if (v1 > v2) //actor is moving closer
	{
		//calculate the the effective mass at contact point for each object.
		//i.e how much the contact point will move due to applied force.
		float mass1 = 1.0f / (1.0f / m_mass + (r1*r1) / m_moment);
		float mass2 = 1.0f / (1.0f / actor2->m_mass + (r2*r2) / actor2->m_moment);

		float elasticity = (m_elasticity + actor2->getElasticity()) / 2.0f;

		vec2 force = (1.0f + elasticity)*mass1*mass2 /
			(mass1 + mass2)*(v1 - v2)*normal;

		float forceMag = glm::length(force);

		if (m_ragDollPart)
		{
			if (forceMag > m_maxForce)
			{
				force *= m_maxForce / forceMag; //cap force
			}
		}
	
		//apply equal and opposite forces
		applyForce(-force, contact - m_position);
		actor2->applyForce(force, contact - actor2->m_position);
	}
}

void Rigidbody::resetPosition()
{
	m_position = m_startPosition;
}


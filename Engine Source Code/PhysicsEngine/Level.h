#pragma once
#include "Rigidbody.h"
#include "PhysicsScene.h"


class Level : public PhysicsScene
{
public:
	Level();
	~Level();


	void CreateLevel1(PhysicsScene* scene);
	void CreateLevel2(PhysicsScene* scene);
	float beatScore = 0;
	bool endLevel  = false;

};


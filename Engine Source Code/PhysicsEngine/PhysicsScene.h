#pragma once
#include <glm/vec2.hpp>
#include <vector>
#include "PhysicsObject.h"
#include <algorithm>
#include <list>
#include "Rigidbody.h"
#include <iostream>
#include "Circle.h"
#include "Plane.h"
#include "Box.h"
#include <glm/ext.hpp>
#include "BoxOBB.h"
#include <Renderer2D.h>
#include "RagDoll.h"
using namespace std;
using namespace glm;
class PhysicsScene
{
public:
	PhysicsScene() : m_timeStep(0.01f), m_gravity(0,0) {  }
	~PhysicsScene();

	void debugScene();
	void addActor(PhysicsObject* actor); //Adding an object to the scene to be updated.
	void removeActor(PhysicsObject* actor); //Delete actor from physics scene
	void update(float deltaTime); //Update physics
	void updateGizmos();
	PhysicsObject* getActor(PhysicsObject* actor); //get reference to a specific actor in scene
	int getActorCount() const { return m_actors.size(); } //get collection amount
	void setGravity(const glm::vec2 gravity) { m_gravity = gravity; } //change the gravity setting.
	glm::vec2 getGravity() const { return m_gravity; } //get the current gravity setting
	void clearScene(); //wipe scene clean.
	void setTimeStep(const float timeStep) { m_timeStep = timeStep; }//set the frequency that physics gets updated.
	float getTimeStep() const { return m_timeStep;} //return the frequency that the physics gets updated.
	void checkCollision(); //Repsonsible for checking collisions between each actor in scene

	/////////////////COLLISION DETECTION FUNCTIONS/////////////////
	static bool planeVsPlane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool planeVsCircle(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool circleVsPlane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool circleVsCircle(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxAABBvsboxAABB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool planevsBoxAABB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxAABBvsPlane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool circlevsBoxAABB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxAABBvsCircle(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxOBBvsboxOBB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxOBBvsboxAABB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxABBvsboxOBB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool planevsBoxOBB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxOBBvsPlane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool circlevsBoxOBB(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool boxOBBvsCircle(PhysicsObject* obj1, PhysicsObject* obj2);
	static void ApplyContactForces(Rigidbody* body1, Rigidbody* body2, vec2 normal, float penetration);
	float getScore() const { return m_score; } //get the current score of the game
	float setScore(int amount) { return m_score += amount; } //Add score
	float m_killableForce = 1500; //the force required for ragdolls to break.


protected:
	vec2 m_gravity;
	float m_timeStep;
	vector<PhysicsObject*> m_actors; //collection of objects in scene list
	const int SHAPE_COUNT = 4;
	int actorCount = 0;
	bool resize = false;
	float m_score = 0;

};


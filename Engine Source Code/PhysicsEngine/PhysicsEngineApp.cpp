#include "PhysicsEngineApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <Gizmos.h>

PhysicsEngineApp::PhysicsEngineApp() {

}

PhysicsEngineApp::~PhysicsEngineApp() {

}

bool PhysicsEngineApp::startup() {
	//increase line count to increase amount of objects we can draw.
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);
	m_2dRenderer = new aie::Renderer2D();
	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);
	menuBackground = new aie::Texture("../bin/textures/mainMenuBackground.png");
	gameBackground = new aie::Texture("../bin/textures/gameBackground.png");
	currentGameState = MAINMENU;
	wood = new aie::Texture("../bin/textures/wood.PNG");



	return true;
}

void PhysicsEngineApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
}

void PhysicsEngineApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	

	switch (currentGameState)
	{
		case MAINMENU:
			
			UINav();

		
			if (input->wasKeyPressed(aie::INPUT_KEY_ENTER))
			{
				if (mainMenuIter == 0)
				{
					currentGameState = LEVELSELECT;
				}
				if (mainMenuIter == 1)
				{
					currentGameState = EXIT;
				}
			}
			
		break;

		case LEVELSELECT:
			UINav();

			if (input->wasKeyPressed(aie::INPUT_KEY_ENTER))
			{
				if (mainMenuIter == 0)
				{
					currentGameState = GAME;
					level = 1;
					intializeGame = true;

				}
				if (mainMenuIter == 1)
				{
					currentGameState = GAME;
					level = 2;
					intializeGame = true;
				}
			}
			break;

		case GAME:
			UpdateGame(deltaTime);
		break;
		case EXIT:
			quit();
		break;
	}

	//UpdateGame(deltaTime);



	
}

void PhysicsEngineApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();
	static float aspectRatio = 16 / 9.f;
	if (!intializeGame)
	{
		aie::Gizmos::draw2D(glm::ortho<float>(-100, 100,
			-100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));
	}
			
	// output some text, uses the last used colour
	ostringstream playerScore;


	switch (currentGameState)
	{
		case MAINMENU:
			m_2dRenderer->drawSprite(menuBackground, this->getWindowWidth() * 0.5f, this->getWindowHeight() * 0.5f, this->getWindowWidth(), this->getWindowHeight());
			m_2dRenderer->drawText(m_font, "Play", this->getWindowWidth() * 0.5f - 50, this->getWindowHeight() * 0.5f - 200);
			m_2dRenderer->drawText(m_font, "Quit", this->getWindowWidth() * 0.5f - 50, this->getWindowHeight() * 0.5f - 250);
			respawnTimer = 0;
			if (mainMenuIter == 0)
			{
				m_2dRenderer->drawCircle(this->getWindowWidth() * 0.5f - 80, this->getWindowHeight() * 0.5f - 190, 15);
			}
			if (mainMenuIter == 1)
			{
				m_2dRenderer->drawCircle(this->getWindowWidth() * 0.5f - 80, this->getWindowHeight() * 0.5f - 240, 15);
			}

		break;

		case LEVELSELECT:
			m_2dRenderer->drawSprite(menuBackground, this->getWindowWidth() * 0.5f, this->getWindowHeight() * 0.5f, this->getWindowWidth(), this->getWindowHeight());
			m_2dRenderer->drawText(m_font, "Level1", this->getWindowWidth() * 0.5f - 50, this->getWindowHeight() * 0.5f - 200);
			m_2dRenderer->drawText(m_font, "Level2", this->getWindowWidth() * 0.5f - 50, this->getWindowHeight() * 0.5f - 250);
			if (mainMenuIter == 0)
			{
				m_2dRenderer->drawCircle(this->getWindowWidth() * 0.5f - 80, this->getWindowHeight() * 0.5f - 190, 15);
			}
			if (mainMenuIter == 1)
			{
				m_2dRenderer->drawCircle(this->getWindowWidth() * 0.5f - 80, this->getWindowHeight() * 0.5f - 240, 15);
			}
		
			break;

		case GAME:
			if (respawnTimer > 10 && !levelManager->endLevel)
			{
				m_2dRenderer->drawText(m_font, "Press R to Respawn", this->getWindowWidth() * 0.5f - 10, this->getWindowHeight() * 0.5f);
			}
			if (!intializeGame)
			{
				playerScore << "Score: " << m_physicsScene->getScore();
				m_2dRenderer->drawText(m_font, playerScore.str().c_str(), this->getWindowWidth() * 0.5f - 950, 2 * this->getWindowHeight() * 0.5f - 30);
				if (levelManager->beatScore == m_physicsScene->getScore())
				{
					levelManager->endLevel = true;
				}
				if (levelManager->endLevel)
				{
					m_2dRenderer->drawText(m_font, "GAME OVER: PRESS ESCAPE TO ENTER MAIN MENU", this->getWindowWidth() * 0.5f - 260, this->getWindowHeight() * 0.5f);
				}
			}
			
			m_2dRenderer->drawText(m_font, "Press ESC to Main Menu", 0, 0);

		break;
		case EXIT:
		break;
	}
	// done drawing sprites
	m_2dRenderer->end();
}

void PhysicsEngineApp::InitializeGame()
{
	this->setBackgroundColour(0, 0.74, 1, 1);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setGravity(vec2(0, -38.f));
	m_physicsScene->setTimeStep(0.01f);

	slingShot[0] = new BoxOBB(vec2(-80, -40), vec2(0, 0), 0, 10000, vec4(160.0f / 255.0f, 82.0f / 255.0f, 45.0f / 255.0f, 1), vec2(2, 10), vec2(0, 0), vec2(0, 0), true, true);
	slingShot[0]->setAngularDrag(0);
	slingShot[0]->setLinearDrag(0);

	ragDoll = new RagDoll(m_physicsScene, vec2(-80.f, -5), vec4(1, 0, 0, 1), false);
	levelManager = new Level();

	//Intialize the level and the enemys
	if (level == 1)
	{
		RagDoll* enemys[3];
		enemys[0] = new RagDoll(m_physicsScene, vec2(50, -30), vec4(0, 1, 0, 1), true);
		enemys[0]->getTorso()->setKinematic(false);
		enemys[1] = new RagDoll(m_physicsScene, vec2(55, 7), vec4(0, 1, 0, 1), true);
		enemys[1]->getTorso()->setKinematic(false);
		enemys[2] = new RagDoll(m_physicsScene, vec2(55, 45.35f), vec4(0, 1, 0, 1), true);
		enemys[2]->getTorso()->setKinematic(false);
		levelManager->CreateLevel1(m_physicsScene);
	}
	if (level == 2)
	{
		RagDoll* enemys[6];
		enemys[0] = new RagDoll(m_physicsScene, vec2(10, -20), vec4(0, 1, 0, 1), true);
		enemys[0]->getTorso()->setKinematic(false);
		enemys[1] = new RagDoll(m_physicsScene, vec2(10, 15), vec4(0, 1, 0, 1), true);
		enemys[1]->getTorso()->setKinematic(false);
		enemys[2] = new RagDoll(m_physicsScene, vec2(65, 14), vec4(0, 1, 0, 1), true);
		enemys[2]->getTorso()->setKinematic(false);
		enemys[3] = new RagDoll(m_physicsScene, vec2(40, 20), vec4(0, 1, 0, 1), true);
		enemys[3]->getTorso()->setKinematic(false);
		enemys[4] = new RagDoll(m_physicsScene, vec2(40, 55.5f), vec4(0, 1, 0, 1), true);
		enemys[4]->getTorso()->setKinematic(false);
		enemys[5] = new RagDoll(m_physicsScene, vec2(65, -20), vec4(0, 1, 0, 1), true);
		enemys[5]->getTorso()->setKinematic(false);
		levelManager->CreateLevel2(m_physicsScene);

	}

	ground = new Plane(vec2(0, 1), -50);




	m_physicsScene->addActor(slingShot[0]);
	m_physicsScene->addActor(ground);

	intializeGame = false;
	//levelManager = new Level();
}

void PhysicsEngineApp::UpdateGame(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	aie::Gizmos::clear();

	if (intializeGame)
	{
		InitializeGame();
		return;
	}

	const float extents = 100;
	const float aspectRatio = 16.f / 9.f;
	float physicsTime = 1;
	int xScreen, yScreen;
	input->getMouseXY(&xScreen, &yScreen);

	xScreen -= getWindowWidth() / 2;
	yScreen -= getWindowHeight() / 2;

	vec2 worldPos(extents * 2 * xScreen / getWindowWidth(),
		yScreen * 2 * extents / (aspectRatio * getWindowHeight()));

	m_mousePosition = worldPos;

	//if left mouse is down
	if (input->isMouseButtonDown(0))
	{
		m_slingShotEnd = m_mousePosition; //slingshot end is mouse position
		if (getSlingShotStart) //if there is no start pos of sling shot
		{
			m_slingShotStart = m_mousePosition; //set it the current mouse position (the moment the left mouse button is clicked.)
			getSlingShotStart = false;
		}
		if (ragDoll->getTorso()->isInside(m_mousePosition)) //mouse is inside the torso of main ragdoll.
		{
			ragDoll->getTorso()->setPosition(m_slingShotEnd); //set the position of the ragdolls torso to the endpos (mouse position), springs pull rest of the body.
			float t = 0; //time
			float dt = 0.5f; //how far to step each iteration
			vec2 totalSling = m_slingShotEnd - m_slingShotStart, normalize(totalSling); //the slings shots vector
			predictedPosition = ragDoll->getTorso()->getPosition(); //set the initial predicted position of the torso to the current torso position.
			predictedVelocity = -totalSling * 0.5f; //set it to the force the sling shot string force is going to apply to the ragdoll
			vec2 force = m_physicsScene->getGravity();

			while (t <= 40) //use the third kinematics equation to predict the trajectory.
			{
				predictedPosition += predictedVelocity * dt * dt; //add the predicted velocity to the current predicted position.
				predictedVelocity += (force / ragDoll->getMass()) * dt * dt; //x = 1/2(a*t^2)
				aie::Gizmos::add2DCircle(predictedPosition, 1, 12, vec4(1, 1, 1, 1));//draw circle
				t += dt; //add to current time
			}
		}
	}
	if (input->isMouseButtonUp(aie::INPUT_MOUSE_BUTTON_LEFT))
	{
		if (!getSlingShotStart)
		{
			if (ragDoll->getTorso()->isInside(m_mousePosition)) //apply the force
			{
				vec2 totalSling = m_slingShotEnd - m_slingShotStart, normalize(totalSling);
				vec2 headContact = vec2(0,0); 
				ragDoll->applyForce(totalSling, m_slingForceMultiplier, headContact);
				respawnNow = true;
			}
		}
		getSlingShotStart = true; //reposition the sling shot
		m_slingShotStart = vec2(0, 0);
		m_slingShotEnd = vec2(0, 0);

	}
	if (respawnNow == true)
	{
		respawnTimer += deltaTime;
		if (respawnTimer > 10)
		{
			if (input->wasKeyPressed(aie::INPUT_KEY_R))
			{
				ragDoll->resetPosition();
				respawnTimer = 0;
				respawnNow = false;
			}
		}
	}

	aie::Gizmos::add2DLine(m_slingShotStart, m_slingShotEnd, vec4(1, 1, 1, 1));



	m_physicsScene->update(deltaTime / physicsTime);
	m_physicsScene->updateGizmos();
	//aabbBox->UpdatePoints();
	//aabbBox2->UpdatePoints();
	aie::Gizmos::add2DAABBFilled(vec2(0, -55), vec2(100, 5), vec4(0.4, 0.2, 0, 1));

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
	{
		this->setBackgroundColour(0, 0, 0, 1);
		m_physicsScene->clearScene();
		currentGameState = MAINMENU;
	}

}

void PhysicsEngineApp::UINav()
{

	aie::Input* input = aie::Input::getInstance();

	if (input->wasKeyPressed(aie::INPUT_KEY_DOWN) || input->wasKeyPressed(aie::INPUT_KEY_S))
	{
		mainMenuIter++;
	}
	if (input->wasKeyPressed(aie::INPUT_KEY_UP) || input->wasKeyPressed(aie::INPUT_KEY_W))
	{
		mainMenuIter--;
	}

	if (mainMenuIter > 1) // if it is greator than max row amount go to the top (0 being the top and 1 being the second), go back to the top.
	{
		mainMenuIter = 0;
	}
	if (mainMenuIter < 0) //do the opposite when going past the top.
	{
		mainMenuIter = 1;
	}
}

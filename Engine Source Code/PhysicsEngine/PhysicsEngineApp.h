#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"
#include "Circle.h"
#include "Plane.h"
#include <glm/ext.hpp>
#include "Box.h"
#include "BoxOBB.h"
#include "Spring.h"
#include "RagDoll.h"
#include <sstream>
#include "Level.h"

enum GameState {
	MAINMENU = 0,
	LEVELSELECT = 1,
	GAME = 2,
	EXIT = 3
};


class PhysicsEngineApp : public aie::Application {
	

public:

	PhysicsEngineApp();
	virtual ~PhysicsEngineApp();
	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();
	void InitializeGame();
	//void IntializeGame();
	void UpdateGame(float deltaTime);
	void UINav();

protected:

	Circle* sphere;
	//BoxOBB* body[5];
	RagDoll* ragDoll;
	BoxOBB* slingShot[2];
	Spring* slingShotString[2];
	Spring* bodyJoints[5];
	Plane* ground;
	BoxOBB* buildingBlocks[20];
	bool fire = false;
	bool getSlingShotStart = true;
	vec2 m_slingShotStart;
	vec2 m_mousePosition;
	vec2 m_slingShotEnd;
	float slingSpeed = 1;
	float m_slingForceMultiplier = 5;
	//vec2 predictedPositions[8];
	vec2 predictedPosition = vec2(0, 0);
	vec2 predictedVelocity = vec2(10, 10);
	float ragDollsTotalMass = 0;
	float respawnTimer = 0;
	bool respawnNow = false;
	int mainMenuIter = 0;
	bool intializeGame = false;
	int level = 0;
	Level* levelManager;
	aie::Texture* menuBackground;
	aie::Texture* gameBackground;
	aie::Texture* wood;
	GameState currentGameState;
	PhysicsScene* m_physicsScene;
	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
};
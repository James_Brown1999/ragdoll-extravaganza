#pragma once
#include "PhysicsObject.h"
#include "Rigidbody.h"
#include <Gizmos.h>
#include <cmath>

class Spring : public PhysicsObject
{
public:
	Spring(Rigidbody* body1, Rigidbody* body2, float restLength,
		float springCoefficient, float damping, vec2 contact1, vec2 contact2) : PhysicsObject(SPRING), m_body1(body1), m_body2(body2), m_restLength(restLength), 
		m_springCoefficient(springCoefficient), m_damping(damping), m_contact1(contact1), m_contact2(contact2){}
	~Spring() {}

	virtual void fixedUpdate(vec2 gravity, float timeStep);
	virtual void debug();
	virtual void makeGizmo();
	float setMaxForce(float maxForce) { return m_maxForce = maxForce; } //clamp the maximum force
	float setMinForce(float minForce) { return m_minForce = minForce; } //clamp the minimum force


protected:
	Rigidbody* m_body1;
	Rigidbody* m_body2;
	bool m_limbJoint; //if the spring is apart of a ragdoll
	vec2 m_contact1; //connection point 1
	vec2 m_contact2; //connection point 2
	float m_maxForce = 0;
	float m_minForce = 0;
	float m_damping;
	float m_restLength; //length of the spring at rest
	float m_springCoefficient; //how stretchy the spring is


};


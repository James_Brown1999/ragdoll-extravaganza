#pragma once
#include "Rigidbody.h"
#include <Gizmos.h>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>

class BoxOBB : public Rigidbody
{
public:
	BoxOBB(vec2 position, vec2 velocity, float angle,
		float mass, vec4 colour, vec2 extents, vec2 localX, vec2 localY, bool kinematic, bool ragDoll) : Rigidbody(BOXOBB,position,velocity, angle,mass,0,kinematic, ragDoll), m_colour(colour), 
		m_extents(extents)
	{
		setMoment(1.0f / 12.0f * mass * getWidth() * getHeight());
		setLocalX(vec2(localX));
		setLocalY(vec2(localY));
	}
	~BoxOBB() {}

	virtual void fixedUpdate(vec2 gravity, float timeStep);
	virtual void makeGizmo();
	virtual bool checkCollision(PhysicsObject* pOther);
	float getWidth() const { return m_extents.x * 2; }
	float getHeight() const { return m_extents.y * 2; }
	vec2 getExtents() const { return m_extents; }

	bool checkBoxCorners(const BoxOBB& box, vec2& contact, int& numContacts, float &penetration, vec2& edgeNormal);
	bool isInside(vec2 point);


protected:
	vec2 m_extents; //half extents
	vec4 m_colour;

	//x and y axes of a matrix (Based on rotation)
	


};


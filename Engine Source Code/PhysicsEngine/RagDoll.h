#pragma once
#include "PhysicsObject.h"
#include "BoxOBB.h"
#include "Circle.h"
#include "Rigidbody.h"
#include "Spring.h"
#include "PhysicsScene.h"
#include <Font.h>

class PhysicsScene;

class RagDoll : public PhysicsObject
{
public:
	RagDoll(PhysicsScene* scene, vec2 position, vec4 color, bool destructible) : PhysicsObject(RAGDOLL), m_currentScene(scene), spawnPosition(position), m_destructible(destructible)
	{
		createRagDoll(scene, position, color);
		setJoints(scene);
	}
	~RagDoll() {}
	virtual void fixedUpdate(vec2 gravity, float timeStep); //update object at a fixed frame rate.
	virtual void debug();
	virtual void makeGizmo();
	void createRagDoll(PhysicsScene* scene, vec2 position, vec4 color); //create the ragdoll.
	void setJoints(PhysicsScene* scene);
	PhysicsScene* getScene() { return m_currentScene; } //get the current physics scene.
	void destroyJoints(PhysicsScene* actor); //disconnect all the joints
	float getMass(); //get the weight of all the limbs
	void applyForce(vec2 force, float multiplier, vec2 contact); //apply force to ragdoll.
	void resetPosition(); //resets the ragdoll to its spawn position.
	Circle* getHead() const { return dynamic_cast<Circle*>(bodyParts[0]); } //cast rigidbody to get Circle collider
	BoxOBB* getTorso() const { return dynamic_cast<BoxOBB*>(bodyParts[1]); }//cast rigidbody to get BoxOBB collider
	BoxOBB* getRightArm() const { return dynamic_cast<BoxOBB*>(bodyParts[2]); }//cast rigidbody to get BoxOBB collider
	BoxOBB* getLeftArm() const { return dynamic_cast<BoxOBB*>(bodyParts[3]); }//cast rigidbody to get BoxOBB collider
	BoxOBB* getRightLeg() const { return dynamic_cast<BoxOBB*>(bodyParts[4]); }//cast rigidbody to get BoxOBB collider
	BoxOBB* getLeftLeg() const { return dynamic_cast<BoxOBB*>(bodyParts[5]); }//cast rigidbody to get BoxOBB collider
	void draw(aie::Renderer2D* renderer2D); //draw ragdoll
	bool isDestructible() { return m_destructible; } //Can the ragdoll joints be destroyed?

protected:
	PhysicsScene* m_currentScene = nullptr; //Scene reference
	Rigidbody* bodyParts[6]; //Ragdolls body parts
	Spring* bodyJoints[5]; //Ragdolls spring joints
	vec2 spawnPosition;
	bool m_destructible = false;


};


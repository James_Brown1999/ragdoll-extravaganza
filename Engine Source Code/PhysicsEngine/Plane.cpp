#include "Plane.h"

void Plane::fixedUpdate(vec2 gravity, float timeStep)
{
}

void Plane::makeGizmo()
{
	float lineSegmentLength = 300;
	vec2 centerPoint = m_normal * m_distanceToOrigin;
	 //rotate normal through 90 degrees around the z axis
	vec2 parallel(m_normal.y, -m_normal.x);
	vec4 colour(1, 1, 1, 1);
	vec2 start = centerPoint + (parallel * lineSegmentLength);
	vec2 end = centerPoint - (parallel * lineSegmentLength);
	aie::Gizmos::add2DLine(start, end, colour);
}

void Plane::resetPosition()
{
	vec2(0,0);
}
void Plane::resolveCollision(Rigidbody * actor2, vec2 contact)
{
	vec2 normal = this->getNormal();
	vec2 relativeVelocity = actor2->getVelocity();

	float elasticity = 1; //e
	float j = dot(-(1 + elasticity) * (relativeVelocity), normal) * actor2->getMass();

	vec2 force = normal * j;
	actor2->applyForce(force, contact - actor2->getPosition());
}
///void Collide(PhysicsObject* other);
//virtual void CollideWithPlane(Plane* other);
//virtual void CollideWithBox(Box* other)
//virtual void CollideWithCircle(Circle* other)


//void Collide(PhysicsObject* other)
//{
//  switch
//    CollideWithBox((Box*) other)
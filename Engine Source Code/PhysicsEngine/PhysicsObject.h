#pragma once
#include <glm/vec2.hpp>
#include <vector>
#include <Renderer2D.h>
using namespace std;
using namespace glm;

enum ShapeType {
	RAGDOLL = -2,
	SPRING = -1,
	PLANE = 0,
	CIRCLE,
	BOX,
	BOXOBB
};

class PhysicsObject
{
protected:
	PhysicsObject(ShapeType a_shapeID) : m_shapeID(a_shapeID) {}
	

public:
	virtual void fixedUpdate(vec2 gravity, float timeStep) = 0;
	virtual void debug() = 0;
	virtual void makeGizmo() = 0;
	ShapeType getShapeID() { return m_shapeID; }
protected:
	ShapeType m_shapeID;
};


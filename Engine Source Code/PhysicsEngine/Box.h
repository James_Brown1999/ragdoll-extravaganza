#pragma once
#include "Rigidbody.h"
#include <glm/vec4.hpp>
#include <Gizmos.h>

class Box : public Rigidbody
{
public:
	Box(vec2 position, vec2 velocity,float mass, float width, float height, vec4 colour, bool kinematic, bool ragDoll) : Rigidbody(BOX,position,velocity,0,mass,0, kinematic, ragDoll),
	m_width(width), m_height(height), m_colour(colour){
		setMoment(1.0f / 12.0f * mass * width * height);
	}
	~Box() {}
	virtual void debug();
	virtual bool checkCollision(PhysicsObject* pOther);

	virtual void makeGizmo();
	float getWidth() { return m_width; }
	float getHeight() { return m_height; }
	vec2 getMin() { return m_min; }
	vec2 getMax() { return m_max; }
	void UpdatePoints();

protected:
	vec2 m_min;
	vec2 m_max;
	float m_width = 0;
	float m_height = 0;
	vec4 m_colour;
	

};


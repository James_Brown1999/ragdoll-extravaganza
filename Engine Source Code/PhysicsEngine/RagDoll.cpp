#include "RagDoll.h"

void RagDoll::fixedUpdate(vec2 gravity, float timeStep)
{
}

void RagDoll::debug()
{
}

void RagDoll::makeGizmo()
{
}

void RagDoll::createRagDoll(PhysicsScene* scene, vec2 position, vec4 color)
{
	//Used my weight and distributed it across each limb/shape. Elasticity must be turned down.
	bodyParts[0] = new Circle(position, vec2(0, 0), 0, 5.84f, 2.5f, color, vec2(0, 0), vec2(0, 0), false, true);//Head
	bodyParts[0]->setLinearDrag(0.3f);
	bodyParts[0]->setAngularDrag(0.3f);
	bodyParts[1] = new BoxOBB(vec2(bodyParts[0]->getPosition().x, bodyParts[0]->getPosition().y - 9), vec2(0, 0), bodyParts[0]->getAngle(), 40, color, vec2(1.5f, 3), vec2(0, 0), vec2(0, 0), true , true); //Torso
	bodyParts[1]->setAngularDrag(0.1f);
	bodyParts[1]->setLinearDrag(0.1f);
	bodyParts[2] = new BoxOBB(vec2(bodyParts[1]->getPosition().x + 5.5, bodyParts[1]->getPosition().y + 3), vec2(0, 0), 0, 2.32f, color, vec2(1, 2), vec2(0, 0), vec2(0, 0), false, true); //Right Arm
	bodyParts[2]->setAngularDrag(0.3f);
	bodyParts[2]->setLinearDrag(0.3f);
	bodyParts[3] = new BoxOBB(vec2(bodyParts[1]->getPosition().x - 5.5, bodyParts[1]->getPosition().y + 3), vec2(0, 0), 0, 2.23f, color, vec2(1, 2), vec2(0, 0), vec2(0, 0), false, true); //Left Arm
	bodyParts[3]->setAngularDrag(0.3f);
	bodyParts[3]->setLinearDrag(0.3f);
	bodyParts[4] = new BoxOBB(vec2(bodyParts[1]->getPosition().x - 1.5f, bodyParts[1]->getPosition().y - 9.5f), vec2(0, 0), 0, 6.392f, color, vec2(0.5f, 2), vec2(0, 0), vec2(0, 0), false, true); //Right Leg
	bodyParts[4]->setAngularDrag(0.3f);
	bodyParts[4]->setLinearDrag(0.3f);
	bodyParts[5] = new BoxOBB(vec2(bodyParts[1]->getPosition().x + 1.5f, bodyParts[1]->getPosition().y - 9.5f), vec2(0, 0), 0, 6.392, color, vec2(0.5f, 2), vec2(0, 0), vec2(0, 0), false, true); // Left Leg
	bodyParts[5]->setAngularDrag(0.3f);
	bodyParts[5]->setLinearDrag(0.3f);

	for (int i = 0; i < 6; i++)
	{
		//Cap the force the prevent ragdoll from spazzing out
		bodyParts[i]->setMaxForce(700);
		//add the limbs to th
		bodyParts[i]->setParent(this);
		scene->addActor(bodyParts[i]);
	}
}

void RagDoll::setJoints(PhysicsScene* scene)
{
	//dynamic casting each limb and naming them correctly to make the code easier to read.
	Circle* head = dynamic_cast<Circle*>(bodyParts[0]);
	BoxOBB* torso = dynamic_cast<BoxOBB*>(bodyParts[1]);
	BoxOBB* rightArm = dynamic_cast<BoxOBB*>(bodyParts[2]);
	BoxOBB* leftArm = dynamic_cast<BoxOBB*>(bodyParts[3]);
	BoxOBB* rightLeg = dynamic_cast<BoxOBB*>(bodyParts[4]);
	BoxOBB* leftLeg = dynamic_cast<BoxOBB*>(bodyParts[5]);

	////ERROR HANDELING
	if (head != nullptr && torso != nullptr && rightArm != nullptr && leftArm != nullptr && rightLeg != nullptr && leftLeg != nullptr)
	{
		//HEAD TO TORSO
		vec2 headJoint = head->getLocalX() * head->getLocalY() - vec2(0, head->getRadius() / 2);
		vec2 torsoHeadJoint = torso->getLocalX() * torso->getLocalY() + vec2(0, torso->getExtents().y);
		bodyJoints[0] = new Spring(head, torso, 1.0f, 400, 50, headJoint, torsoHeadJoint);
		//RIGHT ARM TO TORSO
		vec2 torsoRightArmJoint = torso->getLocalX() * torso->getLocalY() + torso->getExtents();
		vec2 rightArmJoint = rightArm->getLocalX() * rightArm->getLocalY() + vec2(-rightArm->getExtents().x, rightArm->getExtents().y);
		bodyJoints[1] = new Spring(torso, rightArm, 1.0f, 400, 50, torsoRightArmJoint, rightArmJoint);
		//LEFT ARM TO TORSO
		vec2 torsoLeftArmJoint = torso->getLocalX() * torso->getLocalY() + vec2(-torso->getExtents().x, torso->getExtents().y);
		vec2 leftArmJoint = leftArm->getLocalX() * leftArm->getLocalY() + vec2(leftArm->getExtents().x, leftArm->getExtents().y);
		bodyJoints[2] = new Spring(torso, leftArm, 1.0f, 400, 50, torsoLeftArmJoint, leftArmJoint);
		//RIGHT LEG TO TORSO
		vec2 torsoRightLegJoint = torso->getLocalX() * torso->getLocalY() - torso->getExtents();
		vec2 rightLegJoint = rightLeg->getLocalX() * rightLeg->getLocalY() + vec2(0, rightLeg->getExtents().y);
		bodyJoints[3] = new Spring(torso, rightLeg, 1.0f, 400, 50, torsoRightLegJoint, rightLegJoint);
		//LEFT LEG TO TORSO
		vec2 torsoLeftLegJoint = torso->getLocalX() * torso->getLocalY() + vec2(torso->getExtents().x, -torso->getExtents().y);
		vec2 leftLegJoint = leftLeg->getLocalX() * leftLeg->getLocalY() + vec2(0, leftLeg->getExtents().y);
		bodyJoints[4] = new Spring(torso, leftLeg, 1.0f, 400, 50, torsoLeftLegJoint, leftLegJoint);
		for (int i = 0; i < 5; i++)
		{
			//Tightening joints to prevent ragdoll limbs from spazzing.
			bodyJoints[i]->setMaxForce(500.f);
			bodyJoints[i]->setMinForce(-500.f);
			scene->addActor(bodyJoints[i]); //Add spring to physics scene inorder to update physics.
		}
	}
}

void RagDoll::destroyJoints(PhysicsScene * actor)
{
	for (int i = 0; i < 5; i++)
	{
		actor->removeActor(bodyJoints[i]); //remove from physics scene.
	}
	actor->setScore(100);
	m_destructible = false;
}

float RagDoll::getMass()
{
	float totalMass = 0;
	//add all the masses of each limb.
	for (int i = 0; i < 6; i++)
	{
		totalMass += bodyParts[i]->getMass();
	}
	return totalMass;
}

//Designed for launching the ragdoll at start of the game.
void RagDoll::applyForce(vec2 force, float multiplier, vec2 contact)
{
	for (int i = 0; i < 6; i++)
	{
		bodyParts[i]->setKinematic(false); //disable kinematic
		bodyParts[i]->applyForce(-force * bodyParts[i]->getMass() *  multiplier, vec2(0, 0)); //apply force to all the ragdolls limbs in the opposite direction of the slingshot. Pass in zero Vector2 as
		//no physical contact was made.
	}
}

void RagDoll::resetPosition()
{
	//reset to the spawnpositions
	bodyParts[0]->setPosition(spawnPosition); //Head
	bodyParts[1]->setPosition(vec2(bodyParts[0]->getPosition().x, bodyParts[0]->getPosition().y - 9)); //Torso
	bodyParts[1]->setKinematic(true); //Re-enable kinematic state
	bodyParts[2]->setPosition(vec2(bodyParts[1]->getPosition().x + 5.5, bodyParts[1]->getPosition().y + 3)); //Right Arm
	bodyParts[3]->setPosition(vec2(bodyParts[1]->getPosition().x - 5.5, bodyParts[1]->getPosition().y + 3)); //Left Arm
	bodyParts[4]->setPosition(vec2(bodyParts[1]->getPosition().x - 1.5f, bodyParts[1]->getPosition().y - 9.5f)); //Right Leg
	bodyParts[5]->setPosition(vec2(bodyParts[1]->getPosition().x + 1.5f, bodyParts[1]->getPosition().y - 9.5f)); //Left Leg
	for (int i = 0; i < 6; i++)
	{
		bodyParts[i]->setAngle(0); //Straighten joints.
	}
}

void RagDoll::draw(aie::Renderer2D * renderer2D)
{
}

